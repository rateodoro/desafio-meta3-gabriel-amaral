﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Desafio_Meta3.Tests.Negocios
{
    [TestClass]
    public class UsuarioTest
    {
        [TestMethod]
        public void InserirUsuario()
        {
            Modelo.Usuario usuario = new Modelo.Usuario
            {
                Ativo = true,
                Cpf = "00011122233",
                DataInclusao = System.DateTime.Now,
                DataNascimento = System.DateTime.Now,
                Login = "RenatoMoura",
                Senha = "123456",
                Nome = "Renato de Assis Moura"
            };

            Negocio.ControleUsuario.InserirUsuario(usuario);
            Assert.IsNotNull(usuario);
        }

        [TestMethod]
        public void ObterUsuarioPorLogin()
        {
            var usuario = Negocio.ControleUsuario.ObterUsuarioPorLogin("joao");

            Assert.IsNotNull(usuario);
        }
        [TestMethod]
        public void ValidaLogin()
        {
            var valido = Negocio.ControleUsuario.ValidaLogin("joao", "123456");
            var resultadoEsperado = true;
            Assert.AreEqual(valido, resultadoEsperado);
        }
    }
}
