﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Negocio
{
    public static class ControlePessoaFisica
    {
        public static List<PessoaFisica> ObterListaPessoaFisica()
        {
            return Repositorio.PessoaFisicaRepositorio.ObterListaPessoaFisica();
        }

        public static void InserirPessoaFisica(PessoaFisica pessoaFisica)
        {
            Repositorio.PessoaFisicaRepositorio.InserirPessoaFisica(pessoaFisica);
        }

        public static void AtualizarPessoaFisica(PessoaFisica pessoaFisica)
        {
            Repositorio.PessoaFisicaRepositorio.AtualizaPessoaFisica(pessoaFisica);
        }

        public static void DeletarPessoaFisica(int id)
        {
            Repositorio.PessoaFisicaRepositorio.DeletarPessoaFisica(id);
        }

        public static PessoaFisica ObterPessoaFisicaPorId(int id)
        {
            return Repositorio.PessoaFisicaRepositorio.ObterPessoaFisicaPorId(id);
        }
    }
}
