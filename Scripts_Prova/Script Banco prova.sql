IF OBJECT_ID ('dbo.Usuario') IS NOT NULL
	DROP TABLE dbo.Usuario
GO

CREATE TABLE dbo.Usuario
	(
	Id             INT IDENTITY NOT NULL,
	IdPessoaFisica INT NOT NULL,
	Login          VARCHAR (50) NOT NULL,
	Senha          VARCHAR (200) NOT NULL,
	DataInclusao   DATETIME NOT NULL,
	Ativo          BIT NOT NULL,
	CONSTRAINT PK_Usuario PRIMARY KEY (Id),
	CONSTRAINT FK_Usuario_PessoaFisica FOREIGN KEY (IdPessoaFisica) REFERENCES dbo.PessoaFisica (Id)
	)
GO

IF OBJECT_ID ('dbo.PessoaFisica') IS NOT NULL
	DROP TABLE dbo.PessoaFisica
GO

CREATE TABLE dbo.PessoaFisica
	(
	Id              INT IDENTITY NOT NULL,
	Cpf             VARCHAR (11) NOT NULL,
	Nome            VARCHAR (60) NOT NULL,
	DataNascimento  DATETIME NOT NULL,
	Email           VARCHAR (120) NULL,
	TelefoneFixo    VARCHAR (15) NULL,
	TelefoneCelular VARCHAR (15) NULL,
	Sexo            CHAR (1) NULL,
	CONSTRAINT PK_PessoaFisica PRIMARY KEY (Id)
	)
GO

IF OBJECT_ID ('dbo.Funcionario') IS NOT NULL
	DROP TABLE dbo.Funcionario
GO

CREATE TABLE dbo.Funcionario
	(
	Id             INT IDENTITY NOT NULL,
	IdPessoaFisica INT NOT NULL,
	IdFuncao       INT NOT NULL,
	Salario        MONEY NOT NULL,
	DataAdminissao DATETIME NOT NULL,
	DataDemissao   DATETIME NULL,
	Ativo          BIT NOT NULL,
	CONSTRAINT FK_Funcionario_PessoaFisica FOREIGN KEY (IdPessoaFisica) REFERENCES dbo.PessoaFisica (Id),
	CONSTRAINT FK_Funcionario_Funcao FOREIGN KEY (IdFuncao) REFERENCES dbo.Funcao (Id)
	)
GO

IF OBJECT_ID ('dbo.Funcao') IS NOT NULL
	DROP TABLE dbo.Funcao
GO

CREATE TABLE dbo.Funcao
	(
	Id    INT IDENTITY NOT NULL,
	Nome  VARCHAR (50) NOT NULL,
	Ativo BIT NOT NULL,
	CONSTRAINT PK_Funcao PRIMARY KEY (Id)
	)
GO

IF OBJECT_ID ('dbo.Endereco') IS NOT NULL
	DROP TABLE dbo.Endereco
GO

CREATE TABLE dbo.Endereco
	(
	Id             INT IDENTITY NOT NULL,
	IdPessoaFisica INT NOT NULL,
	Cep            VARCHAR (8) NOT NULL,
	Rua            VARCHAR (100) NOT NULL,
	Numero         VARCHAR (30) NOT NULL,
	Bairro         VARCHAR (50) NULL,
	Cidade         VARCHAR (50) NULL,
	Estado         VARCHAR (50) NULL,
	Pais           VARCHAR (50) NULL,
	CONSTRAINT PK_Endereco PRIMARY KEY (Id),
	CONSTRAINT FK_Endereco_PessoaFisica FOREIGN KEY (IdPessoaFisica) REFERENCES dbo.PessoaFisica (Id)
	)
GO

