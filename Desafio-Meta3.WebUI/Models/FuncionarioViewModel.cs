﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desafio_Meta3.WebUI.Models
{
    public class FuncionarioViewModel
    {
        public List<Modelo.Funcionario> FuncionarioLista { get; set; }
        public Modelo.Funcionario Funcionario { get; set; }
        public List<Modelo.Funcao> FuncaoLista { get; set; }
        public List<Modelo.PessoaFisica> PessoaFisicaLista { get; set; }
    }
}