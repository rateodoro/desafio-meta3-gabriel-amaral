﻿using Desafio_Meta3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelo;

namespace Desafio_Meta3.WebUI.Controllers
{
    public class PessoaFisicaController : Controller
    {
        // GET: PessoaFisica
        public ActionResult Index()
        {
            PessoaFisicaViewModel pessoaFisicaViewModel = new PessoaFisicaViewModel
            {
                PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica()
            };
            return View(pessoaFisicaViewModel.PessoaFisicaLista);
        }

        // GET: PessoaFisica/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PessoaFisica/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PessoaFisica/Create
        [HttpPost]
        public ActionResult Create(PessoaFisica pessoaFisica)
        {
            try
            {
                Negocio.ControlePessoaFisica.InserirPessoaFisica(pessoaFisica);
                ViewBag.sucesso = "Registro salvo com sucesso";
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View(pessoaFisica);
            }
        }

        // GET: PessoaFisica/Edit/5
        public ActionResult Edit(int id)
        {
            PessoaFisica pessoaFisica = Negocio.ControlePessoaFisica.ObterPessoaFisicaPorId(id);
            return View(pessoaFisica);
        }

        // POST: PessoaFisica/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PessoaFisica pessoaFisica)
        {
            try
            {
                Negocio.ControlePessoaFisica.AtualizarPessoaFisica(pessoaFisica);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ViewBag.error = e.Message;
                return View(pessoaFisica);
            }
        }

        // GET: PessoaFisica/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PessoaFisica/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, PessoaFisica pessoaFisica)
        {
            try
            {
                Negocio.ControlePessoaFisica.DeletarPessoaFisica(id);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ViewBag.error = e.Message; 
                return View(pessoaFisica);
            }
        }
    }
}
